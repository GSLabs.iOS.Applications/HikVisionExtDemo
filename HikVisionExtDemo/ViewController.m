//
//  ViewController.m
//  HikVisionExtDemo
//
//  Created by Dan on 26.11.2021.
//

#import "ViewController.h"

@interface ViewController () <HveBrowserDelegate>

@property HveBrowser *browser;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    HveClient *client = [HveClient clientWithBase:@"http://192.168.8.1" username:@"admin" password:@"qwerty-123"];
//    [self activateV1:client];
//    [self wirelessUpdate:client];
//    [self ipAddressUpdate:client];
//    [self deviceInfoGet:client];
//    [self streamingChannelUpdate:client];
//    [self timeUpdateV1:client];
    
    self.browser = [HveBrowser browserWithType:@"_psia._tcp"];
    [self.browser.delegates addObject:self];
    [self.browser start];
}

- (void)hveBrowser:(HveBrowser *)sender endpointFound:(HveEndpoint *)endpoint {
    [sender stop];
    
    HveClient *client = [HveClient clientWithEndpoint:endpoint username:@"admin" password:@"qwerty-123"];
//    [self activateV1:client];
//    [self wirelessUpdate:client];
//    [self ipAddressUpdate:client];
//    [self deviceInfoGet:client];
//    [self streamingChannelUpdate:client];
    [self timeUpdateV1:client];
}

- (void)activateV1:(HveClient *)client {
    HveActivateInfo *activateInfo = HveActivateInfo.new;
    activateInfo.password = @"qwerty-123";
    
    [client activateV1Async:activateInfo completion:^(HveResponseStatus *responseStatus, NSError *error) {
        if (responseStatus == nil) {
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            NSLog(@"Activated");
        }
    }];
}

- (void)wirelessUpdate:(HveClient *)client {
    HveWPA *wpa = HveWPA.new;
    wpa.algorithmType = @"TKIP";
    wpa.sharedKey = @"12345678";
    wpa.wpaKeyLength = wpa.sharedKey.length;
    
    HveWirelessSecurity *wirelessSecurity = HveWirelessSecurity.new;
    wirelessSecurity.securityMode = @"WPA2-personal";
    wirelessSecurity.wpa = wpa;
    
    HveWireless *wireless = HveWireless.new;
    wireless.enabled = YES;
    wireless.ssid = @"My network";
    wireless.wirelessSecurity = wirelessSecurity;
    
    [client wirelessUpdateAsync:@"2" wireless:wireless completion:^(HveResponseStatus *responseStatus, NSError *error) {
        if (responseStatus == nil) {
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            NSLog(@"Connecting to %@", wireless.ssid);
        }
    }];
}

- (void)ipAddressUpdate:(HveClient *)client {
    HveIPv6Mode *ipv6Mode = HveIPv6Mode.new;
    ipv6Mode.ipv6AddressingType = @"dhcp";
    
    HveIPAddress *ipAddress = HveIPAddress.new;
    ipAddress.ipVersion = @"dual";
    ipAddress.addressingType = @"dynamic";
    ipAddress.ipv6Mode = ipv6Mode;
    
    [client ipAddressUpdateAsync:@"1" ipAddress:ipAddress completion:^(HveResponseStatus *responseStatus, NSError *error) {
        if (responseStatus == nil) {
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            [client rebootAsync:nil];
        }
    }];
}

- (void)deviceInfoGet:(HveClient *)client {
    [client deviceInfoGetAsync:^(HveDeviceInfo *deviceInfo, NSError *error) {
        if (deviceInfo == nil) {
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            NSLog(@"Model - %@", deviceInfo.model);
        }
    }];
}

- (void)streamingChannelUpdate:(HveClient *)client {
    HveVideo *video = HveVideo.new;
    video.videoCodecType = @"H.264";
    video.videoResolutionWidth = 1920;
    video.videoResolutionHeight = 1080;
    video.videoQualityControlType = @"VBR";
    video.maxFrameRate = 2500;
    
    HveAudio *audio = HveAudio.new;
    audio.enabled = YES;
    audio.audioCompressionType = @"G.726";
    
    HveStreamingChannel *streamingChannel = HveStreamingChannel.new;
    streamingChannel.video = video;
    streamingChannel.audio = audio;
    
    [client streamingChannelUpdateAsync:@"101" streamingChannel:streamingChannel completion:^(HveResponseStatus *responseStatus, NSError *error) {
        if (responseStatus == nil) {
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            NSLog(@"Codec - %@", video.videoCodecType);
        }
    }];
}

- (void)timeUpdateV1:(HveClient *)client {
    [client timeUpdateV1Async:^(HveResponseStatus *responseStatus, NSError *error) {
        if (responseStatus == nil) {
            NSLog(@"Error - %@", error.localizedDescription);
        } else {
            NSLog(@"Time updated");
        }
    }];
}

@end
