//
//  SceneDelegate.h
//  HikVisionExtDemo
//
//  Created by Dan on 26.11.2021.
//

#import "HveMain.h"

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end
